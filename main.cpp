#include <iostream>
#include "IRobot.h"
#include "Robot2D.h"
#include "Robot3D.h"

int main() {
    Robot2D robot2d;
    Robot3D robot3d;

    robot2d.setPosition(1.0, 2.0);
    robot2d.setMotion(3.0, 4.0);

    double x, y, z;
    robot2d.getPosition(x, y, z);
    std::cout << "Robot2D Position: (" << x << ", " << y << ")" << std::endl;

    robot3d.setPosition(1.0, 2.0, 3.0);
    robot3d.setMotion(4.0, 5.0, 6.0);
    robot3d.getPosition(x, y, z);
    std::cout << "Robot3D Position: (" << x << ", " << y << ", " << z << ")" << std::endl;

    return 0;
}
