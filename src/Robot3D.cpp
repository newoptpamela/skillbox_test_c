#include "Robot3D.h"

void Robot3D::setPosition(double x, double y, double z) {
    this->x = x;
    this->y = y;
    this->z = z;
}

void Robot3D::setMotion(double dx, double dy, double dz) {
    this->x += dx;
    this->y += dy;
    this->z += dz;
}

void Robot3D::getPosition(double& x, double& y, double& z) const {
    x = this->x;
    y = this->y;
    z = this->z;
}
