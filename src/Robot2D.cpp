#include "Robot2D.h"

void Robot2D::setPosition(double x, double y, double z) {
    this->x = x;
    this->y = y;
}

void Robot2D::setMotion(double dx, double dy, double dz) {
    this->x += dx;
    this->y += dy;
}

void Robot2D::getPosition(double& x, double& y, double& z) const {
    x = this->x;
    y = this->y;
    z = 0;
}
