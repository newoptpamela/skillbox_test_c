#ifndef ROBOT3D_H
#define ROBOT3D_H

#include "IRobot.h"

class Robot3D : public IRobot {
private:
    double x, y, z;

public:
    Robot3D() : x(0), y(0), z(0) {}
    void setPosition(double x, double y, double z) override;
    void setMotion(double dx, double dy, double dz) override;
    void getPosition(double& x, double& y, double& z) const override;
};

#endif // ROBOT3D_H
