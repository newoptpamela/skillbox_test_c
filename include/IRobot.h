#ifndef IROBOT_H
#define IROBOT_H

class IRobot {
public:
    virtual ~IRobot() = default;
    virtual void setPosition(double x, double y, double z = 0) = 0;
    virtual void setMotion(double dx, double dy, double dz = 0) = 0;
    virtual void getPosition(double& x, double& y, double& z) const = 0;
};

#endif // IROBOT_H
