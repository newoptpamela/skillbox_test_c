#ifndef ROBOT2D_H
#define ROBOT2D_H

#include "IRobot.h"

class Robot2D : public IRobot {
private:
    double x, y;

public:
    Robot2D() : x(0), y(0) {}
    void setPosition(double x, double y, double z = 0) override;
    void setMotion(double dx, double dy, double dz = 0) override;
    void getPosition(double& x, double& y, double& z) const override;
};

#endif // ROBOT2D_H
