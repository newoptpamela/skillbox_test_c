# Объяснение

### Создание объекта `Child ch;`:

- При создании объекта `Child` сначала вызывается конструктор базового класса `Parent`.
- Конструктор `Parent` выводит "Parent()".
- Затем вызывается конструктор класса `Child`.
- Конструктор `Child` выводит "Child()".

### Создание объекта `Parent p;`:

- При создании объекта `Parent` вызывается его конструктор.
- Конструктор `Parent` выводит "Parent()".

### Уничтожение объекта `ch` при завершении `main()`:

- При уничтожении объекта `Child` сначала вызывается деструктор класса `Child`.
- Деструктор `Child` выводит "~Child()".
- Затем вызывается деструктор базового класса `Parent`.
- Деструктор `Parent` выводит "~Parent()".

### Уничтожение объекта `p` при завершении `main()`:

- При уничтожении объекта `Parent` вызывается его деструктор.
- Деструктор `Parent` выводит "~Parent()".

### Итоговый вывод программы

При выполнении программы вывод будет следующим:

- Parent()
- Child()
- Parent()
- ~Child()
- ~Parent()
- ~Parent()

